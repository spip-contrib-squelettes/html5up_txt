# Changelog


## 1.4.7 2025-01-11

### Fix
Fix: Modification du fil d'Ariane dans les pages

## 1.4.6 2025-01-10

### Feat - Fix
f485e4b Feat: Ajout du fil d'Ariane dans les pages
398dc81 Fix: Ajout #LESAUTEURS aux articles du blog
fe0b86c Fix: Correction age des brêves <30 jours pour affichage sommaire + ajout #INTRODUCTION dans les articles du blog
afc0697 Fix: Ajout du second critère de tri par date inverse aux boucles ARTICLES
01fa305 Fix: Correction le libellé h3 du formulaire de recherche dans le footer


## 1.4.5 2024-12-13

### Feat - Fix - Refactor
362ac45 Refactor: Modification affichage brèves dans le sommaire
7196d9f Fix: Affichage des brèves de moins de '30' jours dans le sommaire
f480b35 Feat: Ajout page breve.html
64880d9 Fix: Correction libellé 'Rechercher' en doublon dans la sidebar
a676d43 Fix: Début de commentaire manquant sur une ligne du css txt.spip.css.html

## 1.4.4 2024-11-15

### Feat - Fix - Refactor
Feat: Paramétrage de la taille des images (logo) dans les différentes pages
Refactor: Formulaire de recherche remonté dans la sidebar des articles

## 1.4.3 2024-10-29

### Feat - Fix - Refactor
Feat: Ajout lien espace privé dans le footer
Fix: Le bandeau n'est plus obligatoire dans la configuration du plugin
Refactor: Reorganisation des sections pour sidebar

## 1.4.2 2024-10-13

### Fix - Feat - Refactor
Feat: Ajout Connection/Déconnection dans le footer
Refactor: Modification positionnement formulaire recherche
Fix: Correction appel sidebar dans page auteur


## 1.4.1 2024-10-03

### Fix - Feat
Fix: Correction responsive du footer en mode smartphone
Fix: Barre typo dans textarea du formulaire de configuration
Fix: Suppression class=spip_logo du logo auteur
Feat: Ajout d'articles mis en avant dans le sommaire
Feat: Modification du footer en 4 colonnes pour les plugins Newsletters et Sociaux

## 1.4.0 - 2024-10-01

### Feat

Ajout d'articles mis en avant dans le sommaire
Modification du footer en 4 colonnes pour les plugins Newsletters et Sociaux

## 1.3.3 - 2024-09-30

### Fix
- Correction footer

## 1.3.1 - 2024-09-27

### Feat
- Ajout page auteur + lien dans page article
- Ajout des pages mot, site, recherche, 404
- Ajout d'une sidebar paramétrable

## 1.2.4 - 2024-06-05

### Fix
- ajout |timestamp à perso.css
- necessite le plugin "page unique"

### Feat
- ajout copyright supplémentaire paramétrable
- ajout configuration formulaire Newsletters dans sommaire

## 1.2.3 - 2024-05-07

### Fix

- L'article en avant du blog n'est pas le dernier

## 1.2.2 - 2024-04-03


### Fix

- Issue #1 : Réduire l'image de la bannière
- Le plugin "Page unique" passe en utilise dans paquet.xml


## 1.2.1 - 2024-02-03

### Feat

- filtre text_truncate (sip_bonux obligatoire) pour garder mise en forme du texte de l'article blog en avant du sommaire

### Fix

- apparition sections vides a tort en absence de note et de portfolio page article

### Added

- Ajout d'un `CHANGELOG.md` et mise à jour du `README.md`
