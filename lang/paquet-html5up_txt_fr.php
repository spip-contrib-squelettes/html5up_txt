<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// H
	'html5up_txt_description' => 'Ce plugin permet de reprendre le design TXT proposé sur Html5Up',
	'html5up_txt_nom' => 'Html5Up TXT',
	'html5up_txt_slogan' => 'Squelette adapté du thème TXT de HTML5 UP',
];
