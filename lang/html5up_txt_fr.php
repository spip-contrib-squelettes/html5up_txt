<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'accueil' => 'Accueil',
	'archives' => 'Archives',

	// B
	'bouton_subscribe' => 'S\'abonner',

	// C
	'contact' => 'Contact',
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',

	// E
    'explication_articles_home_avant' => "La page sommaire affiche les rubriques de premier niveau. Vous pouvez avoir besoin d'ajouter un ou des articles AVANT cette liste de rubriques." ,
    'explication_articles_home_apres' => "La page sommaire affiche les rubriques de premier niveau. Vous pouvez avoir besoin d'ajouter un ou des articles APRES cette liste de rubriques." ,
    'explication_articles_en_avant' => "Vous pouvez ajouter des articles qui seront mis en avant (2 maximum)." ,
	'explication_config_contenu' => "Le squelette utilise les sous titres pour les articles, n'oubliez pas de les activer dans <a href='?exec=configurer_contenu'>configurer contenu</a>",
	'explication_config_titre' => "Nom du site apparait dans l'image sur fond opaque",
	'explication_doc_bandeau' => "Document qui sera utilisé pour l'image du bandeau. Le thème original utilise une image de taille <b>2000 × 656 pixels</b> ",
	'explication_text_sommaire' => "Texte du surtitre des rubriques (qui prendra la balise H2). 'Rubriques' par défaut",
	'explication_text_en_avant' => "Texte du surtitre des articles mis en avant (qui prendra la balise H2). Absent par défaut",
	'explication_text_blog' => "Texte du surtitre du blog (qui prendra la balise H2). 'Articles les plus récents' par défaut",
	'explication_text_sous_rubrique' => "Texte du surtitre des sous-rubriques (qui prendra la balise H2). 'Rubriques' par défaut",
	'explication_text_rubrique' => "Texte du surtitre des articles (qui prendra la balise H2). 'Articles' par défaut",
	'explication_text_article' => "Texte du surtitre des articles de la même rubrique (qui prendra la balise H2). 'Dans la même rubrique' par défaut",
	'explication_text_breve' => "Texte du surtitre des brèves de la même rubrique (qui prendra la balise H2). 'Autres brèves' par défaut",
	'explication_long_intro' => "Longueur de l'introduction des articles dans les vignettes",
	'explication_rubrique_blog' => "Rubrique(s) présentée(s) sous forme de blog dans le bas de la page.",
	'explication_header' => "Texte apparaissant dans le l'entête (facultatif)",
	'explication_footer' => "Texte apparaissant dans le pied de page",
	'explication_copyright' => "Copyright supplémentaire de pied de page",
	'explication_couleur' => "Couleur du thème (Menu, boutons ...). Par défaut #b1ddab",
	'explication_auteur' => "Affichage de l'auteur dans les articles qui renvoie vers la page auteur",
	'explication_configuration' => "Configuration des pages",
	'explication_largeur_image' => "Largeur des images d'en-tête des pages (par défaut 800 px). Cette valeur sera reprise dans le ratio des miniatures",
	'explication_hauteur_image' => "Hauteur des images d'en-tête des pages (par défaut 230 px). Cette valeur sera reprise dans le ratio des miniatures",


	// H
	'html5up_txt_titre' => 'Configurer html5up_txt',	
	
	
	// L
    'label_articles_home_avant' => "Article(s) avant liste des secteurs",
    'label_articles_home_apres' => "Article(s) après liste des secteurs",
    'label_articles_en_avant' => "Article(s) mis en avant",
	'label_config_contenu' => "Sous-titre",
	'label_config_titre' => 'Nom site sur fond opaque',
	'label_doc_bandeau' => "Image bandeau",
	'label_text_sommaire' => "Surtitre des rubriques",
	'label_text_en_avant' => "Surtitre des articles mis en avant",
	'label_text_sous_rubrique' => "Surtitre des sous-rubriques",
	'label_text_rubrique' => "Surtitre des articles",
	'label_text_article' => "Surtitre des articles de la même rubrique",
	'label_text_breve' => "Surtitre des brèves de la même rubrique",
	'label_text_blog' => "Surtitre du blog",
	'label_long_intro' => "Longueur de l'introduction",
	'label_rubrique_blog' => "Rubrique(s) du blog",
	'label_header' => "En-tête",
	'label_footer' => "Pied de page",
	'label_copyright' => "Copyright",
	'label_couleur' => "Couleur",
	'label_configuration' => "Configuration",
	'legend' => "Préambule",
	'label_largeur_image' => "Largeur image",
	'label_hauteur_image' => "Hauteur image",
	'legend_sommaire' => "Paramétrage page Sommaire ",
	'legend_rubrique' => "Paramétrage page Rubrique ",
	'legend_article' => "Paramétrage page Article ",
	'legend_breve' => "Paramétrage page Brève ",
	'legend_header' => "Paramétrage Header",
	'legend_footer' => "Paramétrage Footer",
	'legend_couleur' => "Choix couleur",	
	'lire_suite' => "Lire la suite",
	'label_auteur' => "Affichage de l'auteur",


	// N
	'nous_suivre' => 'Nous suivre',

	// R
	'read_more' => 'Lire la suite',
	'reseau_sociaux' => 'Réseaux sociaux',

	// T
	'newsletter' => "Newsletter",
	'texte_calltoaction' => "Pour avoir des nouvelles régulières, inscrivez votre mail ",
	'texte_config_titre' => "Le nom du site sur un fond opaque",
	'texte_auteur' => "Affichage de l'auteur",
	'titre_configurer_html5up_txt' => 'Configurer Html5up TXT',
	'titre_page_configurer_html5up_txt' => 'Configurer Html5up TXT',
	'titre_page_configurer_html5up_txt' => 'Configurer Html5up TXT',

	
	// H
	'html5up_txt_titre' => 'Html5Up TXT',


);
